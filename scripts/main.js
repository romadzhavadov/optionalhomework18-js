const student = {
  name: 'Roman',
  lastName: 'Dzhavadov',
  tabel: {
    math: 5,
    biology: 9,
    physics: 12,
  },

  showBadGrades(obj) {
    
    let count = 0;

    for(let key in obj) {
      if (obj[key] < 4) {
        count++
      } 
    } 
    if (count === 0) {
      alert("Студент переведено на наступний курс");
    }
    console.log(`У студента ${this.name} ${this.lastName} - ${count} оцінок (менше 4)`);

  }, 
}

const copyObjectInfo = (obj) => {
  const copyObject = {}
  for (let key in obj) {
    
    if (typeof obj[key] === 'object' && obj[key] !== null) {
      copyObject[key] = copyObjectInfo(obj[key]);
    } else {
      copyObject[key] = obj[key];
    }

  }
  return copyObject
}  

let clone = copyObjectInfo(student);

clone.name = 'Petr';
clone.tabel.biology = 25;

console.log(student);
console.log(clone);


